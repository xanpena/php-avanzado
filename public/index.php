<?php 

ini_set('display_errors', 1);
ini_set('display_starup_error', 1);
error_reporting(E_ALL);

require_once '../vendor/autoload.php'; 

use Illuminate\Database\Capsule\Manager as Capsule;
use Aura\Router\RouterContainer;
use WoohooLabs\Harmony\Harmony;
use WoohooLabs\Harmony\Middleware\DispatcherMiddleware;
use WoohooLabs\Harmony\Middleware\HttpHandlerRunnerMiddleware;
use Zend\Diactoros\ServerRequestFactory;
use Zend\Diactoros\Response;
use Zend\HttpHandlerRunner\Emitter\SapiEmitter;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

$dotenv = Dotenv\Dotenv::create(__DIR__ . '/..');
$dotenv->load();

// create a log channel
$log = new Logger('app');
$log->pushHandler(new StreamHandler(__DIR__.'../logs/app.log', Logger::WARNING));

$container = new DI\Container();
$capsule = new Capsule;

$capsule->addConnection([
    'driver'    => 'mysql',
    'host'      => getenv('DB_HOST'),
    'database'  => getenv('DB_DATABASE'),
    'username'  => getenv('DB_USER'),
    'password'  => '',
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '',
]);

// Make this Capsule instance available globally via static methods... (optional)
$capsule->setAsGlobal();

// Setup the Eloquent ORM... (optional; unless you've used setEventDispatcher())
$capsule->bootEloquent();

$request = Zend\Diactoros\ServerRequestFactory::fromGlobals(
    $_SERVER,
    $_GET,
    $_POST,
    $_COOKIE,
    $_FILES
);

// var_dump($request->getUri()->getPath());

$routerContainer = new RouterContainer();
$map = $routerContainer->getMap();

$map->get('index', '/php-avanzado/', [
    'App\Controllers\IndexController',
    'indexAction'
]);

$map->get('indexJob', '/php-avanzado/jobs/', [
    'App\Controllers\JobController',
    'indexAction'
]);

$map->get('deleteJob', '/php-avanzado/jobs/{id}/delete', [
    'App\Controllers\JobController',
    'deleteAction'
]);

$map->get('addJob', '/php-avanzado/jobs/add', [
    'App\Controllers\JobController',
    'getAddJobAction'
]);

$map->post('saveJob', '/php-avanzado/jobs/add', [
    'App\Controllers\JobController',
    'getAddJobAction'
]);

$matcher = $routerContainer->getMatcher();
$route = $matcher->match($request);

try{
    $harmony = new Harmony($request, new Response());
    $harmony
        ->addMiddleware(new HttpHandlerRunnerMiddleware(new SapiEmitter()))
        ->addMiddleware(new \Franzl\Middleware\Whoops\WhoopsMiddleware)
        ->addMiddleware(new Middlewares\AuraRouter($routerContainer))
        ->addMiddleware(new DispatcherMiddleware($container, 'request-handler'))
        ->run();
} catch (Exception $e){
    $log->warning($e->getMessage());
   // $emitter = new SapiEmitter();
   // $emitter->emit(new Response\EmptyResponse(400));
} catch(Error $e) {
    $log->error('job not found');
    $emitter = new SapiEmitter();
    $emitter->emit(new Response\EmptyResponse(500));
}

 