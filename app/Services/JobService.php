<?php

namespace App\Services;

use \App\Models\Job;

class JobService {
    
    public function delete($id) {
        $job = Job::find($id);
        if(!$job){
            throw new \Exception('Job not Found');
        }
        $job->delete();
    }
}