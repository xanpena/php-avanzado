<?php 

namespace App\Controllers;

use \App\Models\{Job, Project};

class IndexController extends BaseController {

    public function indexAction() {
        $jobs = Job::all();
        $project1 = new Project('Project 1', 'Description project 1.', true, 3);
        $projects = [
            $project1
        ];

        $jobs = array_filter($jobs->toArray(), function($job) {
            return $job['months'] >= 1;
        });

        $name = 'Xan Pena';

        return $this->renderHTML('index.twig', [
            'name' => $name,
            'jobs' => $jobs
        ]);
    }
}



