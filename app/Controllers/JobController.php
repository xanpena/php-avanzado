<?php

namespace App\Controllers;

use App\Models\Job;
use Respect\Validation\Validator as v;
use Respect\Validation\Exceptions\NestedValidationException;
use Zend\Diactoros\Response\RedirectResponse;
use \App\Services\JobService;


class JobController extends BaseController {

    private $jobService;

    public function __construct(JobService $jobService = NULL) {

        parent::__construct();
        $this->jobService = $jobService;
    }

    public function indexAction() {
        // $jobs = Job::whitTrashed()->get();
        $jobs = Job::all();
        return $this->renderHTML('jobs/index.twig', compact('jobs'));
    }

    public function deleteAction($request) {
        $params = $request->getQueryParams();
        $this->jobService->delete($params['id']);

        return new RedirectResponse('jobs');
    }

    public function getAddJobAction($request) {
        
        if($request->getMethod() == 'POST'){
            /*var_dump($_POST);
            var_dump($request->getMethod());
            var_dump($request->getBody());
            var_dump($request->getParsedBody());*/

            $postData = $request->getParsedBody();

            // https://respect-validation.readthedocs.io

            $jobValidator = v::key('title', v::stringType()->length(1, 32))
                ->key('description', v::stringType()->length(1, 150));
            
            try{
                $jobValidator->assert($postData);

                $files = $request->getUploadedFiles();
                //var_dump($files); exit;
                $logo = $files['logo'];

                if($logo->getError() == UPLOAD_ERR_OK){
                    $fileName = $logo->getClientFilename();
                    $logo->moveTo("uploads/$filename");
                }

                $job = new Job();
                $job->title = $postData['title'];
                $job->description = $postData['description'];
                $job->save();

            }catch(NestedValidationException  $exception) {
                $responseMessage = $exception->getMainMessage();
                var_dump($exception->getFullMessage());
            }
            
        }

        return $this->renderHTML('addJob.twig');
    }
    
}